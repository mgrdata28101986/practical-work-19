﻿#include <string>
#include <iostream>
using std::endl;
using std::cout;
using std::string;

class Animal
{
    public:

        virtual void Voice()
        {
            cout << "I say " << ":\"I\'am animal\"" << endl;
        }
};

class Dog : public Animal
{
    void Voice() override
    {
        cout << "I say " << "WOOF!" << endl;
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        cout << "I say " << "MEOW!" << endl;
    }
};

class Horse : public Animal
{
    void Voice() override
    {
        cout << "I say " << "NEIGH!" << endl;
    }
};

int main()
{
    Animal* animalsArray[3];

    Dog*    ptrDog      = new Dog();
    Cat*    ptrCat      = new Cat();
    Horse*  ptrHorse    = new Horse();

    animalsArray[0] = ptrDog;
    animalsArray[1] = ptrCat;
    animalsArray[2] = ptrHorse;

    for (int i = 0; i < 3; i++)
    {
        animalsArray[i]->Voice();
    }
}
